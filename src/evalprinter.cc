#include <sstream>
#include <iostream>
#include <stdio.h>
#include <stdarg.h>
#include "evalprinter.hh"

EvalPrinter::EvalPrinter ()
  : tree_ (NULL)
{
}

void EvalPrinter::push (std::string pref,
			int mid,
			std::string suff)
{
  EvalNode* node = new EvalNode ();
  std::ostringstream str;
  str << pref;
  if (mid > -1)
    str << mid << suff;
  node->content = str.str ();
  if (!stack_.empty ())
    stack_.back ()->childs.push_back (node);
  stack_.push_back (node);
  if (!tree_)
    tree_ = node;
}

void EvalPrinter::pop (bool leaf, bool match)
{
  if (!stack_.empty ())
  {
    EvalNode* node = stack_.back ();
    if (leaf && match)
      node->match = true;
    else
    {
      node->match = false;
      std::list<EvalNode*>& lst = node->childs;
      std::list<EvalNode*>::iterator it = lst.begin ();
      for (; it != lst.end () && !node->match; ++it)
	if ((*it)->match)
	  node->match = true;
    }
    stack_.pop_back ();
  }
}

void EvalPrinter::indent ()
{
  if (!first_)
  {
    std::list<unsigned>::iterator sz = lsizes_.begin ();
    std::list<bool>::iterator vs = lvisib_.begin ();
    std::list<bool>::iterator valid = lvalid_.begin ();
    unsigned count = 0;
    for (; sz != lsizes_.end (); ++sz, ++vs, ++valid, ++count)
    {
      for (unsigned i = 0; i < *sz - 1; ++i)
	*os_ << " ";
      if (*valid)
	*os_ << (char) 0x1B << "[32m";
      if (count != lsizes_.size () - 1)
	*os_ << (*vs ? "│" : " ");
      else
	*os_ << (last_ ? "└" : "├");
      if (*valid)
	*os_ << (char) 0x1B << "[0m";
    }
  }
  else
    first_ = false;
}

void EvalPrinter::visit (EvalNode* node)
{
  bool back = lvalid_.back ();
  if (node->match)
    lvalid_.back () = true;
  indent ();
  lvalid_.back () = back;
  unsigned size = node->content.size () + 2;
  lvalid_.push_back (false);
  if (node->match)
    *os_ << (char) 0x1B << "[32m";
  *os_ << "─" << node->content;
  lvisib_.push_back (false);
  lsizes_.push_back (size);
  if (node->childs.empty ())
    *os_ << "─" << (node->match ? "OK" : "KO") << std::endl;
  else if (node->childs.size () == 1)
    *os_ << "─";
  else
    *os_ << "┬";
  if (node->match)
    *os_ << (char) 0x1B << "[0m";
  if (!node->childs.empty ())
  {
    lvisib_.back () = true;
    std::list<EvalNode*>& lst = node->childs;
    std::list<EvalNode*>::iterator it = lst.begin ();
    unsigned count = 0;
    unsigned lastvalid = 0;
    first_ = true;
    last_ = false;
    for (; it != lst.end (); ++it, ++count)
      if ((*it)->match)
	lastvalid = count;
    it = lst.begin ();
    count = 0;
    for (; it != lst.end (); ++it, ++count)
    {
      if (count == lst.size () - 1)
      {
	last_ = true;
	lvisib_.back () = false;
      }
      else
	last_ = false;
      if (count < lastvalid)
	lvalid_.back () = true;
      else
	lvalid_.back () = false;
      visit (*it);
    }
  }
  lsizes_.pop_back ();
  lvisib_.pop_back ();
  lvalid_.pop_back ();
}

void EvalPrinter::print (std::ostream& os)
{
  os_ = &os;
  visit (tree_);
}
