#include "matcher.hh"
#include "options.hh"
#include "evalprinter.hh"
#include "evalerror.hh"
#include <deque>
#include <iostream>

Matcher::Matcher (Options& opts)
  : opts_ (opts),
    cursor_ (0),
    match_ (true),
    matched_ (false),
    evalPrinter_ (NULL),
    ref_ (false),
    id_ (-1),
    matchOnce (opts.matchOnce ()),
    onlyMatch (opts.onlyMatch ()),
    copyString (opts.onlyMatch ())
{
  if (opts.evalPrint ())
    evalPrinter_ = new EvalPrinter ();
}

Matcher::~Matcher ()
{
}

inline
bool Matcher::good () const
{
  return (cursor_ < input_->size ());
}

bool Matcher::match (unsigned id, Node* ast, const std::string& str)
{
  bool match = false;
  unsigned cursor = 0;
  if (!matches_.empty ())
    matches_.clear ();
  input_ = &str;
  id_ = id;

  while (cursor_ < input_->size () && (!matchOnce || !match))
  {
    matched_ = false;
    ref_ = false;
    match_ = true;
    cursor_ = cursor;
    if (!output_.empty ())
      output_.clear ();
    if (evalPrinter_)
    {
      delete evalPrinter_;
      evalPrinter_ = new EvalPrinter ();
    }
    ast->accept (*this);
    if (evalPrinter_)
      evalPrinter_->print (std::cout);
    if (!output_.empty ())
      matches_.push_back (output_);
    cursor += (output_.empty () ? 1 : output_.size ());
    match |= match_;
  }
  return match;
}

const std::list<std::string>& Matcher::list () const
{
  return matches_;
}

void Matcher::visit (Container& cont)
{
  if (evalPrinter_)
    evalPrinter_->push ("(", cont.get_id (), ")");
  //INFINITE_LOOP_LOCK (cont);
  const std::deque<Node*>& alts = cont.get_list ();
  std::deque<Node*>::const_iterator it = alts.begin ();
  int backup = cursor_;
  if (!ref_)
  {
    if (cont.next)
      successors_.push_back (cont.next);
  }
  else
    ref_ = false;
  std::list<Node*> succBackup = successors_;
  bool match = false;
  if (it < alts.end ())
  {
    std::string strBackup;
    std::string previous;
    std::string longest;
    if (copyString)
    {
      strBackup = output_;
      previous = output_;
      longest = output_;
    }
    do
    {
      successors_ = succBackup;
      cursor_ = backup;
      if (copyString)
	output_ = strBackup;
      match_ = true;
      (*it)->accept (*this);
      match |= match_;
      ++it;
      if (copyString)
      {
	if (output_.size () > previous.size ())
	  longest = output_;
	previous = output_;
      }
    }
    while (it < alts.end () && (!matchOnce || !match_));
    if (copyString)
      output_ = longest;
  }
  else
    match_ = false;
  successors_ = succBackup;
  if (cont.next && !successors_.empty ())
    successors_.pop_back ();
  if (evalPrinter_)
    evalPrinter_->pop (false, match_);
  if (match)
    match_ = true;
}

void Matcher::visit (Any& any)
{
  if (evalPrinter_)
    evalPrinter_->push (".");

  if (!good ())
    match_ = false;
  else
    ++cursor_;
  MATCH_NEXT (any);
}

void Matcher::visit (Begin& begin)
{
  if (evalPrinter_)
    evalPrinter_->push ("^");

  if (cursor_ != 0)
    match_ = false;
  MATCH_NEXT (begin);
}

void Matcher::visit (End& end)
{
  if (evalPrinter_)
    evalPrinter_->push ("$");

  if (good ())
    match_ = false;
  MATCH_NEXT (end);
}

void Matcher::visit (Nothing& node)
{
  MATCH_NEXT (node);
}

void Matcher::visit (Word& node)
{
  if (evalPrinter_)
    evalPrinter_->push (node.get_str ());

  const std::string& str = node.get_str ();
  std::string::const_iterator it = str.begin ();

  while (it < str.end () && good ())
  {
    if (match_ && *it != input_->at (cursor_))
      match_ = false;
    ++it;
    ++cursor_;
  }
  if (it != str.end ())
    match_ = false;
  MATCH_NEXT (node);
}

void Matcher::visit (Brackets& brack)
{
  if (evalPrinter_)
    evalPrinter_->push (good ()
			? std::string ("[") + input_->at (cursor_) + "]"
			: "[]");
  if (!good ())
  {
    match_ = false;
  }
  else
  {
    const std::string& singles = brack.get_singles ();
    const std::string& couples = brack.get_couples ();
    bool found = false;
    unsigned i = 0;

    while (!found && i < singles.size ())
    {
      if (input_->at (cursor_) == singles[i])
	found = true;
      ++i;
    }
    i = 0;
    while (!found && i < couples.size ())
    {
      if (input_->at (cursor_) >= couples[i]
	  && input_->at (cursor_) <= couples[i + 1])
	found = true;
      i += 2;
    }
    if (!found)
      match_ = false;
    else
      ++cursor_;
  }
  MATCH_NEXT (brack);
}

void Matcher::visit (Sequence& node)
{
  unsigned start = node.get_start ();
  unsigned end = node.get_end ();

  if (evalPrinter_)
  {
    std::ostringstream str;
    str << "{" << (int) start << "," << (int) end << "}";
    evalPrinter_->push (str.str ());
  }

  if (node.next)
    successors_.push_back (node.next);
  std::list<Node*> succBackup = successors_;
  unsigned cursBackup = cursor_;
  unsigned count = 0;
  Node* child = node.get_child ();

  child->hasMatched = true;
  for (unsigned i = start; i <= end && child->hasMatched; ++i)
  {
    for (unsigned j = start; j < i; ++j)
      successors_.push_back (child);
    child->accept (*this);
    successors_ = succBackup;
    cursor_ = cursBackup;
    if (match_)
      count = i;
    else
      match_ = true;
  }
  if (count == 0 && start != 0)
    match_ = false;

  if (node.next)
    successors_.pop_back ();

  if (evalPrinter_)
    evalPrinter_->pop (node.next, match_);
}

void Matcher::visit (Reference& ref)
{
  Container* cont = ref.get_ref ();

  if (ref.next)
    successors_.push_back (ref.next);
  ref_ = true;
  if (cont->get_list ().back ())
    cont->accept (*this);
  else
  {
    std::ostringstream stream;
    stream << "inexistant dynamic reference: #" << ref.get_id ();
    throw EvalError (stream.str ());
  }
  if (ref.next)
    successors_.pop_back ();
}
