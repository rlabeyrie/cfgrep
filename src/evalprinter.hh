#ifndef EVALPRINTER_HH_
# define EVALPRINTER_HH_

# include <sstream>
# include <list>
# include <deque>
# include <string>

struct EvalNode
{
  std::list<EvalNode*> childs;
  std::string content;
  bool match;
};

class EvalPrinter
{
public:
  EvalPrinter ();
public:
  void push (std::string, int i = -1, std::string s = std::string (""));
  void pop (bool leaf = false, bool match = false);
  void print (std::ostream&);
private:
  void indent ();
  void visit (EvalNode*);
private:
  std::ostream* os_;
  std::list<EvalNode*> stack_;
  std::list<unsigned> lsizes_;
  std::list<bool> lvisib_;
  std::list<bool> lvalid_;
  EvalNode* tree_;
  bool first_;
  bool last_;
};

#endif /* !EVALPRINTER_HH_ */
