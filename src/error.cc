#include <iostream>
#include "error.hh"

Error::Error ()
{
}

Error::Error (std::string prefix, std::string txt)
  : prefix_ (prefix),
    text_ (txt)
{
}

void Error::print () const
{
  std::cout << "cfgrep: " << prefix_ << text_ << std::endl;
}
