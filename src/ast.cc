#include "ast.hh"
#include "visitor.hh"
#include "syntaxerror.hh"

Node::Node ()
  : trace (-1),
    fileID (-1),
    next (NULL)
{
}

void Any::accept (Visitor& v)
{
  v.visit (*this);
}

void Begin::accept (Visitor& v)
{
  v.visit (*this);
}

void Nothing::accept (Visitor& v)
{
  v.visit (*this);
}

void End::accept (Visitor& v)
{
  v.visit (*this);
}

void Word::accept (Visitor& v)
{
  v.visit (*this);
}

void Container::accept (Visitor& v)
{
  v.visit (*this);
}

void Brackets::accept (Visitor& v)
{
  v.visit (*this);
}

void Sequence::accept (Visitor& v)
{
  v.visit (*this);
}

void Reference::accept (Visitor& v)
{
  v.visit (*this);
}

Word::Word (std::string& str)
  : str_ (str)
{
}

const std::string& Word::get_str () const
{
  return str_;
}

Container::Container (unsigned id)
  : id_ (id)
{
  new_alternative ();
}

unsigned Container::get_id () const
{
  return id_;
}

const std::deque<Node*>& Container::get_list () const
{
  return lst_;
}

void Container::new_alternative ()
{
  lst_.push_back (NULL);
  temp_ = NULL;
  prevtemp_ = NULL;
}

void Container::push (Node* node)
{
  if (dynamic_cast <Sequence*> (node))
  {
    if (!temp_)
      throw SyntaxError ("Sequence must be applied to something");
    ((Sequence*) node)->assign (temp_);
    if (prevtemp_)
      prevtemp_->next = node;
    else
      lst_.back () = node;
    temp_ = node;
  }
  else
  {
    prevtemp_ = temp_;
    if (temp_)
      temp_ = temp_->next = node;
    else
      temp_ = lst_.back () = node;
  }
}

Sequence::Sequence (int start, int end)
  : start_ (start),
    end_ (end),
    node_ (0)
{
}

int Sequence::get_start () const
{
  return start_;
}

int Sequence::get_end () const
{
  return end_;
}

Brackets::Brackets (bool bang, std::string& singles,
		    std::string& couples)
  : bang_ (bang),
    singles_ (singles),
    couples_ (couples)
{
}

bool Brackets::is_neg () const
{
  return bang_;
}

const std::string& Brackets::get_singles () const
{
  return singles_;
}

const std::string& Brackets::get_couples () const
{
  return couples_;
}

Reference::Reference (unsigned id, Container* ref)
  : id_ (id),
    ref_ (ref)
{
}

unsigned Reference::get_id () const
{
  return id_;
}

Container* Reference::get_ref () const
{
  return ref_;
}

void Sequence::assign (Node* node)
{
  node_ = node;
}

Node* Sequence::get_child () const
{
  return node_;
}
