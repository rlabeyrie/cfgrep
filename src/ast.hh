#ifndef AST_HH_
# define AST_HH_

# include <deque>
# include <string>
# include <list>

// forward decs
class Visitor;

class Node
{
public:
  Node ();
  virtual void accept (Visitor&) = 0;
public:
  unsigned trace;
  unsigned fileID;
  std::list<Node*> context;
  Node* next;
  bool hasMatched;
};

class Any : public Node
{
public:
  virtual void accept (Visitor&);
};

class Begin : public Node
{
public:
  virtual void accept (Visitor&);
};

class Nothing : public Node
{
public:
  virtual void accept (Visitor&);
};

class End : public Node
{
public:
  virtual void accept (Visitor&);
};

class Word : public Node
{
public:
  Word (std::string& str);
  virtual void accept (Visitor&);
  const std::string& get_str () const;
private:
  std::string str_;
};

class Container : public Node
{
public:
  Container (unsigned);
  virtual void accept (Visitor&);
  const std::deque<Node*>& get_list () const;
  unsigned get_id () const;
  void push (Node*);
  void new_alternative ();
private:
  std::deque<Node*> lst_;
  unsigned id_;
  Node* temp_;
  Node* prevtemp_;
};

class Brackets : public Node
{
public:
  Brackets (bool, std::string&, std::string&);
  virtual void accept (Visitor&);
  bool is_neg () const;
  const std::string& get_singles () const;
  const std::string& get_couples () const;
private:
  bool bang_;
  std::string singles_;
  std::string couples_;
};

class Sequence : public Node
{
public:
  Sequence (int, int);
  virtual void accept (Visitor&);
  int get_start () const;
  int get_end () const;
  void assign (Node*);
  Node* get_child () const;
private:
  int start_;
  int end_;
  Node* node_;
};

class Reference : public Node
{
public:
  Reference (unsigned, Container*);
  virtual void accept (Visitor&);
  unsigned get_id () const;
  Container* get_ref () const;
private:
  unsigned id_;
  Container* ref_;
};

#endif /* !AST_HH_ */
