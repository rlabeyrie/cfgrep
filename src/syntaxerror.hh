#ifndef SYNTAXERROR_HH_
# define SYNTAXERROR_HH_

# include "error.hh"

class SyntaxError : public Error
{
public:
  SyntaxError (std::string);
};

#endif /* !SYNTAXERROR_HH_ */
