#include "lexer.hh"
#include <sstream>

Lexer::Lexer (const std::string& input)
  : input_ (input),
    peek_ (NONE)
{
  it_ = input_.begin ();
}

Lexer::Token Lexer::peek ()
{
  if (!good ())
    return NONE;
  if (peek_ == NONE)
  {
    if (*it_ == '\\')
    {
      peek_ = CHAR;
      return CHAR;
    }
    else
    {
      const char* lst = "()[]{}*+.?#^$|";
      for (int i = 0; lst[i]; ++i)
	if (lst[i] == *it_)
	{
	  peek_ = static_cast<Lexer::Token> (*it_);
	  return peek_;
	}
      peek_ = CHAR;
      return CHAR;
    }
    return CHAR;
  }
  else return peek_;
}

Lexer::Token Lexer::get (int num)
{
  if (!good ())
    return NONE;
  peek_ = NONE;
  for (int i = 0; i < num - 1; ++i)
    ++it_;
  character = *it_;
  ++it_;
  if (character == '\\')
  {
    character = *it_;
    ++it_;
    return CHAR;
  }
  else
  {
    if (character == '#')
    {
      std::string num;
      for (character = *it_;
	   character >= '0' && character <= '9';
	   character = *(++it_))
	num += character;
      integer = -1;
      if (num != "")
      {
	std::istringstream stream (num);
	stream >> integer;
      }
      return SHARP;
    }
    const char* lst = "()[]{}*+.?#^$|";
    for (int i = 0; lst[i]; ++i)
      if (lst[i] == character)
	return static_cast<Lexer::Token> (character);
    return CHAR;
  }
  return Lexer::CHAR;
}

bool Lexer::good () const
{
  return (it_ != input_.end ());
}
