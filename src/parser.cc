#include <string>
#include <sstream>
#include "parser.hh"
#include "lexer.hh"
#include "ast.hh"
#include "syntaxerror.hh"
#include "options.hh"
#include <iostream>

Parser::Parser (Options& opts)
  : lexer_ (new Lexer (opts.pattern ())),
    ast_ (NULL),
    containerCount_ (0)
{
}

Node* Parser::parse_word ()
{
  lexer_->get ();
  std::string str (&(lexer_->character), 1);

  while (lexer_-> good () && lexer_->peek () == Lexer::CHAR)
  {
    lexer_->get ();
    str += lexer_->character;
  }
  return new Word (str);
}

Node* Parser::parse_parenthesis ()
{
  Container* ret;
  if (++containerCount_ < containerStack_.size ())
  {
    if (containerStack_[containerCount_] != NULL)
      ret = containerStack_[containerCount_];
    else
    {
      containerStack_[containerCount_] = new Container (containerCount_);
      ret = containerStack_[containerCount_];
    }
  }
  else
  {
    ret = new Container (containerCount_);
    containerStack_.push_back (ret);
  }
  lexer_->get ();
  Lexer::Token token = Lexer::NONE;
  while (lexer_->good () && token != Lexer::RPARENT)
  {
    if (token == Lexer::PIPE)
    {
      ret->new_alternative ();
      token = lexer_->get ();
      if (token == Lexer::RPARENT)
      {
	ret->push (new Nothing ());
	break;
      }
    }
    ret->push (parse_exp ());
    token = lexer_->peek ();
  }
  if (lexer_->good ())
    lexer_->get ();
  else
    throw SyntaxError ("Expecting `)' before End Of File");
  return ret;
}

Node* Parser::parse_brackets ()
{
  char previous;
  bool bang = false;
  bool link = false;
  Lexer::Token token;
  std::string singles;
  std::string couples;

  token = lexer_->get (2);
  if (lexer_->character == '^')
  {
    bang = true;
    token = lexer_->get ();
  }
  previous = lexer_->character;
  while (lexer_->good () && (token = lexer_->get ()) != Lexer::RBRACKET)
  {
    if (link)
    {
      couples += previous;
      couples += lexer_->character;
      link = false;
      previous = 0;
    }
    else
    {
      if (lexer_->character == '-' && previous)
	link = true;
      else
      {
	if (previous)
	  singles += previous;
	previous = lexer_->character;
      }
    }
  }
  if (previous)
    singles += previous;
  if (token != Lexer::RBRACKET)
    throw SyntaxError ("Expecting `]' before End Of File");
  return new Brackets (bang, singles, couples);
}

Node* Parser::parse_accolades ()
{
  Lexer::Token token = lexer_->get ();
  std::string num;
  int start = -2;
  int end = -2;
  bool comma = false;

  while (lexer_->good () && (token = lexer_->get ()) != Lexer::RACC)
  {
    if (lexer_->character >= '0' && lexer_->character <= '9')
      num += lexer_->character;
    else if (lexer_->character == ',')
    {
      if (start == -2)
      {
	if (num != "")
	{
	  std::istringstream stream (num);
	  stream >> start;
	}
	else
	  start = 0;
      }
      else
	throw SyntaxError ("Three or more fields between accolades.");
      num = "";
      comma = true;
    }
    else
      throw SyntaxError (std::string ("Unexpected symbol between accolades: `")
			 + lexer_->character + "'");
  }
  if (!comma)
    end = start;
  else if (num != "")
  {
    std::istringstream stream (num);
    stream >> end;
  }

  return new Sequence (start, end);
}

Node* Parser::parse_sharp ()
{
  if (lexer_->integer < 0)
    throw SyntaxError ("Expecting integer after `#'");
  Container* cont;
  if (lexer_->integer < (int) containerStack_.size ())
  {
    if (containerStack_[lexer_->integer] != NULL)
      cont = containerStack_[lexer_->integer];
    else
    {
      containerStack_[lexer_->integer] = new Container (lexer_->integer);
      cont = containerStack_[lexer_->integer];
    }
  }
  else
  {
    while ((int) containerStack_.size () < lexer_->integer)
      containerStack_.push_back (NULL);
    containerStack_.push_back (new Container (lexer_->integer));
    cont = containerStack_.back ();
  }
  return new Reference (lexer_->integer, cont);
}

Node* Parser::parse_exp ()
{
  switch (lexer_->peek ())
  {
    case Lexer::CHAR:
      return parse_word ();
    case Lexer::LPARENT:
      return parse_parenthesis ();
    case Lexer::LBRACKET:
      return parse_brackets ();
    case Lexer::LACC:
      return parse_accolades ();
    case Lexer::WILDCARD:
      lexer_->get ();
      return new Sequence (0, -2);
    case Lexer::PLUS:
      lexer_->get ();
      return new Sequence (1, -2);
    case Lexer::DOT:
      lexer_->get ();
      return new Any ();
    case Lexer::INTERR:
      lexer_->get ();
      return new Sequence (0, 1);
    case Lexer::SHARP:
      lexer_->get ();
      return parse_sharp ();
    case Lexer::BEGIN:
      lexer_->get ();
      return new Begin ();
    case Lexer::END:
      lexer_->get ();
      return new End ();
    default:
      lexer_->get ();
      throw SyntaxError (std::string ("Unexpected symbol `")
			 + lexer_->character + "'");
  }
}

Node* Parser::parse ()
{
  Container* ret = new Container (0);
  containerStack_.push_back (ret);
  while (lexer_->good ())
    ret->push (parse_exp ());
  return ret;
}
