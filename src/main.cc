#include <string>
#include <iostream>
#include <fstream>
#include "parser.hh"
#include "prettyprinter.hh"
#include "matcher.hh"
#include "error.hh"
#include "options.hh"

int read_stream (int& id, std::istream& is,
		 Options& opts, Matcher* matcher, Node* ast)
{
  int ret = 1;
  std::string buffer;
  bool quiet = opts.quiet ();
  bool onlyMatch = opts.onlyMatch ();

  while (is.good () && (!quiet || ret))
  {
    getline (is, buffer);
    if (!is.good ())
      break;
    if (matcher->match (id, ast, buffer))
    {
      ret = 0;
      if (!quiet)
      {
	if (onlyMatch)
	{
	  const std::list<std::string>& lst = matcher->list ();
	  std::list<std::string>::const_iterator it = lst.begin ();
	  for (; it != lst.end (); ++it)
	    std::cout << *it << std::endl;
	}
	else
	  std::cout << buffer << std::endl;
      }
    }
    ++id;
  }
  return ret;
}

int main (int argc, char** argv)
{
  Options opts (argc, argv);
  if (opts.pattern () == "")
    return 42;
  Parser parser  = Parser (opts);
  try
  {
    Node* ast = parser.parse ();
    if (opts.astPrint ())
    {
      Visitor* printer = new PrettyPrinter (ast);
      printer->visit (*ast);
      delete printer;
    }
    else
    {
      Matcher* matcher = new Matcher (opts);
      int ret = 1;
      int id = 0;
      if (opts.files ().empty ())
	ret = read_stream (id, std::cin, opts, matcher, ast);
      else
      {
	const std::list<std::string>& files = opts.files ();
	std::list<std::string>::const_iterator it = files.begin ();
	for (; it != files.end (); ++it)
	{
	  std::ifstream is;
	  is.open ((*it).c_str ());
	  if (!is.good ())
	    std::cerr << "cfgrep: error: cannot open file `" << *it
		      << "'" << std::endl;
	  else
	  {
	    ret &= read_stream (id, is, opts, matcher, ast);
	  }
	}
      }
      delete matcher;
      return ret;
    }
  }
  catch (Error& error)
  {
    error.print ();
    return 42;
  }
  return 0;
}
