#ifndef VISITOR_HXX_
# define VISITOR_HXX_

inline
void Visitor::visit (Node& node)
{
  node.accept (*this);
}

#endif /* !VISITOR_HXX_ */
