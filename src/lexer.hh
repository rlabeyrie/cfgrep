#ifndef LEXER_HH_
# define LEXER_HH_

# include <string>

class Lexer
{
public:
  enum Token
  {
    NONE = 0,
    ID = 1,
    CHAR = 2,
    LPARENT = '(',
    RPARENT = ')',
    LBRACKET = '[',
    RBRACKET = ']',
    LACC = '{',
    RACC= '}',
    WILDCARD = '*',
    PLUS = '+',
    DOT = '.',
    INTERR = '?',
    SHARP = '#',
    BEGIN = '^',
    END = '$',
    PIPE = '|'
  };
public:
  Lexer (const std::string&);
  Token get (int num = 1);
  Token peek ();
  bool good () const;
public:
  int integer;
  char character;
private:
  std::string input_;
  std::string::iterator it_;
  Token peek_;
};

#endif /* !LEXER_HH_ */
