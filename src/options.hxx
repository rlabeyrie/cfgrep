#ifndef OPTIONS_HXX_
# define OPTIONS_HXX_

inline
bool Options::astPrint () const
{
  return astPrint_;
}

inline
bool Options::evalPrint () const
{
  return evalPrint_;
}

inline
bool Options::leftRecurs () const
{
  return leftRecurs_;
}

inline
bool Options::matchOnce () const
{
  return matchOnce_;
}

inline
bool Options::quiet () const
{
  return quiet_;
}

inline
bool Options::onlyMatch () const
{
  return onlyMatch_;
}

inline
const std::string& Options::pattern () const
{
  return pattern_;
}

inline
const std::list<std::string>& Options::files () const
{
  return files_;
}

#endif /* !OPTIONS_HXX_ */
