#include "syntaxerror.hh"

SyntaxError::SyntaxError (std::string str)
  : Error ("Syntax error: ", str)
{
}
