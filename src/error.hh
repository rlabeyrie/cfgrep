#ifndef ERROR_HH_
# define ERROR_HH_

# include <string>

class Error
{
public:
  void print () const;
protected:
  Error ();
  Error (std::string, std::string);
private:
  std::string prefix_;
  std::string text_;
};

#endif /* !ERROR_HH_ */
