#ifndef EVALERROR_HH_
# define EVALERROR_HH_

# include "error.hh"

class EvalError : public Error
{
public:
  EvalError (std::string);
};

#endif /* !EVALERROR_HH_ */
