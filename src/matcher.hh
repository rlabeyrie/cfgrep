#ifndef MATCHER_HH_
# define MATCHER_HH_

# include "visitor.hh"
# include <string>
# include <list>

# define INFINITE_LOOP_LOCK(node)      				\
  if (id_ == (node).fileID && (((node).trace == cursor_	      	\
	      && (matched_ || (node).context == successors_)))) \
  {							       	\
    match_ = false;					       	\
    if (evalPrinter_)						\
      evalPrinter_->pop (true, false);				\
    return;						       	\
  }							       	\
  (node).trace = cursor_;				       	\
  (node).context = successors_;					\
  (node).fileID = id_;

# define MATCH_NEXT(node)					\
  if (match_)							\
  {								\
    (node).hasMatched = true;					\
    if (cursor_ - 1 < input_->size () && copyString)		\
      output_ += input_->at (cursor_ - 1);			\
    if (node.next)						\
    {								\
      node.next->accept (*this);				\
      if (evalPrinter_)						\
	evalPrinter_->pop (false, true);			\
    }								\
    else if (!successors_.empty ())				\
    {								\
      Node* temp = successors_.back ();				\
      successors_.pop_back ();					\
      temp->accept (*this);					\
      if (evalPrinter_)						\
	evalPrinter_->pop (false, true);			\
    }								\
    else							\
    {								\
      matched_ = true;						\
      if (evalPrinter_)						\
	evalPrinter_->pop (true, true);				\
    }								\
  }								\
  else								\
  {								\
    (node).hasMatched = false;					\
    if (!successors_.empty ())					\
      successors_.pop_back ();					\
    if (evalPrinter_)						\
      evalPrinter_->pop (true, false);				\
  }

// forward decs
class Options;
class EvalPrinter;

class Matcher : public Visitor
{
public:
  Matcher (Options&);
  ~Matcher ();
  bool match (unsigned, Node*, const std::string&);
  const std::list<std::string>& list () const;
  virtual void visit (Any&);
  virtual void visit (Word&);
  virtual void visit (Begin&);
  virtual void visit (Nothing&);
  virtual void visit (End&);
  virtual void visit (Container&);
  virtual void visit (Brackets&);
  virtual void visit (Sequence&);
  virtual void visit (Reference&);
private:
  bool good () const;
  void visit_list (Node*);
private:
  Options& opts_;
  unsigned cursor_;
  const std::string* input_;
  bool match_;
  bool matched_;
  std::list<Node*> successors_;
  EvalPrinter* evalPrinter_;
  bool ref_;
  unsigned id_;
  bool matchOnce;
  bool onlyMatch;
  bool copyString;
  std::list<std::string> matches_;
  std::string output_;
};

#endif /* !MATCHER_HH_ */
