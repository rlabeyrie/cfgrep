#include "prettyprinter.hh"
#include <iostream>
#include <string>
#include <stdio.h>

#define RESET		0
#define BRIGHT 		1
#define DIM		2
#define UNDERLINE 	3
#define BLINK		4
#define REVERSE		7
#define HIDDEN		8

#define BLACK 		0
#define RED		1
#define GREEN		2
#define YELLOW		3
#define BLUE		4
#define MAGENTA		5
#define CYAN		6
#define	WHITE		7

namespace
{
  inline
  void set_color (int attr = RESET, int fg = WHITE)
  {
    printf ("%c[%d;%dm", 0x1B, attr, fg + 30);
  }
} // anonymous

PrettyPrinter::PrettyPrinter (bool recurs)
  : level_ (0),
    first_ (false),
    last_ (false),
    recurs_ (recurs)
{
}

void PrettyPrinter::visit (Any&)
{
  indent ();
  std::cout << ". (any character)" << std::endl;
}

void PrettyPrinter::visit (Begin&)
{
  indent ();
  std::cout << "^ (begin)" << std::endl;
}

void PrettyPrinter::visit (Nothing&)
{
  indent ();
  std::cout << "(nothing)" << std::endl;
}

void PrettyPrinter::visit (End&)
{
  indent ();
  std::cout << "$ (end)" << std::endl;
}

void PrettyPrinter::visit (Container& node)
{
  indent ();
  const std::deque<Node*>& lst = node.get_list ();
  set_color (BRIGHT, RED);
  std::cout << "(" << node.get_id () << ")";
  set_color (RESET);
  std::cout << "─";
  levels_.push_back (true);
  levelsId_.push_back (node.get_id ());
  int sz = 1;
  for (int i = node.get_id () / 10; i > 0; i /= 10)
    ++sz;
  levelsSize_.push_back (sz + 3);
  if (lst.size () == 1 && !(lst[0]->next))
    std::cout << "─";
  else
    std::cout << "┬";
  ++level_;
  first_ = true;
  if (lst.size () > 1)
    show_alternatives (lst, node.get_id ());
  else
    show_list (lst[0]);
  --level_;
  levels_.pop_back ();
  levelsSize_.pop_back ();
  levelsId_.pop_back ();
}

void PrettyPrinter::visit (Sequence& node)
{
  indent ();
  unsigned sz = 0;
  set_color (DIM, GREEN);
  sz += printf ("%d", node.get_start ());
  set_color (RESET);
  std::cout << " to ";
  set_color (DIM, GREEN);
  if (node.get_end () >= 0)
    sz += printf ("%d", node.get_end ());
  else
  {
    std::cout << "∞";
    sz++;
  }
  set_color (RESET);
  std::cout << " of──";
  ++level_;
  first_ = true;
  levels_.push_back (false);
  levelsSize_.push_back (sz + 8);
  node.get_child ()->accept (*this);
  levelsSize_.pop_back ();
  levels_.pop_back ();
  --level_;
}

void PrettyPrinter::show_alternatives (const std::deque<Node*>& lst,
				       unsigned id)
{
  for (unsigned alt = 0; alt < lst.size (); ++alt)
  {
    if (alt == lst.size () - 1)
    {
      last_ = true;
      levels_.back () = false;
    }
    else
      last_ = false;
    bool first = first_;
    indent ();
    if (!first)
      printf ("────or ");
    else
      printf ("either ");
    set_color (DIM, YELLOW);
    unsigned sz = printf ("%d:%d", id, alt);
    set_color (RESET);
    if (!(lst[alt]->next))
      printf("──");
    else
      printf("─┬");
    first_ = true;
    ++level_;
    levels_.push_back (true);
    levelsSize_.push_back (sz + 8);
    show_list (lst[alt]);
    levelsSize_.pop_back ();
    levels_.pop_back ();
    --level_;
  }
}

void PrettyPrinter::show_list (Node* lst)
{
  while (lst)
  {
    if (!lst->next)
    {
      last_ = true;
      levels_.back () = false;
    }
    else
      last_ = false;
    lst->accept (*this);
    lst = lst->next;
  }
}

void PrettyPrinter::visit (Brackets& node)
{
  indent ();
  std::cout << "[] ";
  if (node.is_neg ())
    std::cout << "n";
  const std::string& singles = node.get_singles ();
  const std::string& couples = node.get_couples ();
  if (singles.size () + couples.size () <= 1
      || (couples.size () == 2 && singles.size () == 0))
    std::cout << "must be ";
  else
    std::cout << "either ";
  for (unsigned i = 0; i < singles.size (); ++i)
  {
    if (i != 0)
      std::cout << ((node.is_neg ()) ? ", nor " : ", or ");
    set_color (DIM, CYAN);
    std::cout << singles[i];
    set_color (RESET);
  }
  for (unsigned i = 0; i < couples.size (); i += 2)
  {
    if (i != 0 || singles.size () != 0)
      std::cout << ((node.is_neg ()) ? ", nor " : ", or ");
    set_color (DIM, CYAN);
    std::cout << couples[i] << "-" << couples[i + 1];
    set_color (RESET);
  }
  std::cout << std::endl;
}

void PrettyPrinter::visit (Word& node)
{
  indent ();
  std::cout << "word \"";
  set_color (BRIGHT, CYAN);
  std::cout << node.get_str ();
  set_color (RESET);
  std::cout << "\"" << std::endl;
}

void PrettyPrinter::visit (Reference& node)
{
  indent ();
  set_color (DIM, RED);
  std::cout << "#" << node.get_id ();
  set_color (RESET);
  for (unsigned i = 0; i < levelsId_.size (); ++i)
    if (node.get_id () == levelsId_[i])
    {
      std::cout << " (recursive call)" << std::endl;
      return;
    }
  std::cout << "─";
  levels_.push_back (false);
  first_ = true;
  int sz = 1;
  for (int i = node.get_id () / 10; i > 0; i /= 10)
    ++sz;
  levelsSize_.push_back (sz + 1);
  ++level_;
  visit (*(node.get_ref ()));
  levelsSize_.pop_back ();
  levels_.pop_back ();
  --level_;
}

void PrettyPrinter::indent ()
{
  if (first_)
  {
    std::cout << "─";
    first_ = false;
  }
  else if (level_ > 0)
  {
    for (unsigned i = 0; (int) i < (int) level_ - 1; ++i)
    {
      if (i > 0)
	std::cout << " ";
      for (int j = 0; j < levelsSize_[i]; ++j)
	std::cout << " ";
      if (levels_[i])
	std::cout << "│";
      else
	std::cout << " ";
    }
    for (int j = 0; j < levelsSize_.back (); ++j)
      std::cout << " ";
    if (level_ > 1)
      std::cout << " ";
    if (last_)
    {
      std::cout << "└";
      last_ = false;
    }
    else
      std::cout << "├";
    std::cout << "─";
  }
}
