#ifndef PRETTYPRINTER_HH_
# define PRETTYPRINTER_HH_

# include "visitor.hh"
# include <deque>

class PrettyPrinter : public Visitor
{
public:
  PrettyPrinter (bool);
  virtual void visit (Any&);
  virtual void visit (Word&);
  virtual void visit (Begin&);
  virtual void visit (Nothing&);
  virtual void visit (End&);
  virtual void visit (Container&);
  virtual void visit (Brackets&);
  virtual void visit (Sequence&);
  virtual void visit (Reference&);
private:
  void indent ();
  void show_alternatives (const std::deque<Node*>&, unsigned);
  void show_list (Node*);
private:
  unsigned level_;
  std::deque<bool> levels_;
  std::deque<int> levelsSize_;
  std::deque<unsigned> levelsId_;
  bool first_;
  bool last_;
  bool recurs_;
};

#endif /* PRETTYPRINTER_HH_ */
