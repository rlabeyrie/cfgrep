#ifndef VISITOR_HH_
# define VISITOR_HH_

# include "ast.hh"

class Visitor
{
public:
  void visit (Node&);
  virtual void visit (Any&) = 0;
  virtual void visit (Word&) = 0;
  virtual void visit (Begin&) = 0;
  virtual void visit (Nothing&) = 0;
  virtual void visit (End&) = 0;
  virtual void visit (Container&) = 0;
  virtual void visit (Brackets&) = 0;
  virtual void visit (Sequence&) = 0;
  virtual void visit (Reference&) = 0;
};

# include "visitor.hxx"

#endif /* !VISITOR_HH_ */
