#include "evalerror.hh"

EvalError::EvalError (std::string str)
  : Error::Error ("Evaluation error: ", str)
{
}
