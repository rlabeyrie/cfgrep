#ifndef PARSER_HH_
# define PARSER_HH_

# include <string>
# include <deque>

// forward decs
class Node;
class Container;
class Lexer;
class Options;

class Parser
{
public:
  Parser (Options&);
  Node* parse ();
private:
  Node* parse_exp ();
  Node* parse_word ();
  Node* parse_parenthesis ();
  Node* parse_brackets ();
  Node* parse_accolades ();
  Node* parse_sharp ();
private:
  Lexer* lexer_;
  Node* ast_;
  unsigned containerCount_;
  std::deque<Container*> containerStack_;
};

#endif /* !PARSER_HH_ */
