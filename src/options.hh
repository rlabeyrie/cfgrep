#ifndef OPTIONS_HH_
# define OPTIONS_HH_

# include <string>
# include <list>

class Options
{
public:
  Options (int argc, char** argv);
public:
  bool astPrint () const;
  bool evalPrint () const;
  bool leftRecurs () const;
  bool matchOnce () const;
  bool quiet () const;
  bool onlyMatch () const;
  const std::string& pattern () const;
  const std::list<std::string>& files () const;
private:
  bool astPrint_;
  bool evalPrint_;
  bool leftRecurs_;
  bool matchOnce_;
  bool quiet_;
  bool onlyMatch_;
  std::string pattern_;
  std::list<std::string> files_;
};

# include "options.hxx"

#endif /* !OPTIONS_HH_ */
