#include <iostream>
#include <getopt.h>
#include <string.h>
#include "options.hh"

Options::Options (int argc, char** argv)
  : astPrint_ (false),
    evalPrint_ (false),
    leftRecurs_ (false),
    matchOnce_ (false),
    onlyMatch_ (false)
{
  int c;
  int index = 0;
  bool matchPlural = false;
  bool matchOnce = false;

  enum e_opts
  {
    PRINT_AST = 'p',
    PRINT_EVAL = 'd',
    LEFT_RECURS = 'l',
    QUIET = 'q',
    ONLY_MATCH = 'o',
    MATCH_ONCE = 256,
    MATCH_ALL
  };
  struct option opts[] =
  {
    { "print-ast", 0, 0, PRINT_AST },
    { "print-eval", 0, 0, PRINT_EVAL },
    { "left-recursion", 0, 0, LEFT_RECURS },
    { "match-once", 0, 0, MATCH_ONCE },
    { "match-all", 0, 0, MATCH_ALL },
    { "quiet", 0, 0, QUIET },
    { "only-matching", 0, 0, ONLY_MATCH },
    { 0, 0, 0, 0 }
  };
  while ((c = getopt_long (argc, argv, "pdloq", opts, &index)) > 0)
  {
    switch (c)
    {
      case PRINT_AST:
	astPrint_ = true;
	break;
      case PRINT_EVAL:
	evalPrint_ = true;
	break;
      case LEFT_RECURS:
	leftRecurs_ = true;
	break;
      case MATCH_ONCE:
	matchOnce = true;
	break;
      case MATCH_ALL:
	matchOnce_ = false;
	matchPlural = true;
	break;
      case QUIET:
	quiet_ = true;
	break;
      case ONLY_MATCH:
	onlyMatch_ = true;
	break;
    }
  }
  if (optind < argc)
    pattern_ = argv[optind++];
  while (optind < argc)
    files_.push_back (argv[optind++]);
  if (matchOnce)
    matchOnce_ = true;
  if (!matchPlural && files_.size () <= 1)
    matchOnce_ = true;
  if (onlyMatch_ && !matchOnce)
    matchOnce_ = false;
}
